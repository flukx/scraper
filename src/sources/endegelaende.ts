import { ScrapeFunction, ScrapedEvent } from '../index'
import * as moment from 'moment'
import Logger from '@trichter/logger'
import fetch from '../fetch'
import { isRegExp } from 'util'
const log = Logger('trichter-scraper/endegelaende')
import * as cheerio from 'cheerio'


const crawl: ScrapeFunction = async (options: {city: string}) => {
  if(!options.city) {
      log.error(new Error('scraper needs a city'))
      return []
  }
  try {
    const res = await fetch('https://www.ende-gelaende.org/termine/')
    const body = await res.text()
    console.log(options.city)
    const m = body.match(new RegExp('<h3>'+options.city+'[\\s\\S]+?</ul>', 'm'))
    const links = m[0].match(/href=".*?"/g).map(s => s.slice(6, s.length-1))

    let out = []
    for(let link of links) {
        const res = await fetch(link)
        const $ = cheerio.load(await res.text())
        const title = $('.post__title').text().trim()
        const data = $('.post__address').prev().text().trim()
        const desc = $('.post__content').text().trim()
        const d = data.split('      ')

        const start = moment(`${d[0]}, ${d[2]}`, 'D. MMMM YYYY, HH:mm').toISOString()

        const event: ScrapedEvent = {
            id: link.split('/')[4],
            title: title,
            start: start,
            end: null,
            address: d[3],
            link: link,
            description: desc.replace(/\n---/m, '\n\n---') // add a line before dashes to avoid markdown header interpretation
        }
        out.push(event)
        
    }
    return out
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl
