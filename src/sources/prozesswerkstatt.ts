import * as moment from 'moment'
import { ScrapeFunction, ScrapedEvent } from '../index'
import Logger from '@trichter/logger'
import fetch from '../fetch'
import * as cheerio from 'cheerio'

import * as showdown from 'showdown'
import * as jsdom from 'jsdom'
import { URL } from 'url'
import { basename } from 'path'

const log = Logger('trichter-scraper/prozesswerkstatt')
const converter = new showdown.Converter({
  encodeEmails: false
})
const dom = new jsdom.JSDOM()

const crawl: ScrapeFunction = async (_: any) => {
  moment.locale('de')

  try {
    const events: ScrapedEvent[] = []
    const res = await fetch('https://www.prozesswerkstatt-leipzig.org/workshops-seminare/')
    const $ = cheerio.load(await res.text())
    const teaser: {[url: string]: string} = {}
    $('.eael-elements-flip-box-rear-container').each((_, el) => {
      const t = $('.eael-elements-flip-box-content', el)
      const url = $('a.flipbox-button', el)[0].attribs.href
      teaser[url] = t.text().trim()
    })

    for (const url in teaser) {
      try {
        const e = await scrapeEvent(url)
        e.teaser = teaser[url] // add teaser
        events.push(e)
      } catch (err) {
        log.error('unexpected error', err)
      }
    }
    return events
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl

async function scrapeEvent (url: string): Promise<ScrapedEvent> {
  log.info('get ' + url)
  const res = await fetch(url)
  const body = await res.text()
  const $ = cheerio.load(body)

  const title = $($('h1')[0]).text()
  const widgets = $('.elementor-widget-wrap')

  let startDate: moment.Moment
  let endDate: moment.Moment
  let address: string

  let description = ''

  for (const widget of widgets.toArray()) {
    if ($(widget).html().match(/Anmelden unter/)) {
      const headings = $('.elementor-widget-heading', widget)
      address = $(headings[2]).text().trim().replace(/^(in|im) /, '')

      // parse date
      const rawDate = $(headings[0]).text().trim()
      const date = rawDate
        .replace(/\s+und\s.*(\.[0-9]{4})/, '$1') // if two dates, remove second one

      let d: any
      // eslint-disable-next-line no-cond-assign
      if (d = date.match(/^(\d{1,2}).-(\d{1,2}).(\d{1,2}).(\d{4})/)) {
        startDate = moment(`${d[4]}-${d[3]}-${d[1]}T12:00:00`)
        endDate = moment(`${d[4]}-${d[3]}-${d[2]}T12:00:00`)
      } else {
        d = date.match(/^(\d{1,2}).(\d{1,2}).(\d{4})/)
        startDate = moment(`${d[3]}-${d[2]}-${d[1]}T12:00:00`)
        endDate = moment(startDate)
      }

      // parse time
      const rawTime = $(headings[1]).text().trim()
      const t = rawTime.match(/(\d{1,2})-(\d{1,2}) Uhr/)
      if (!t) {
        throw new Error("couldn't find a time here")
      }
      startDate = startDate
        .hours(parseInt(t[0]))
      endDate = endDate
        .hours(parseInt(t[1]))

      // parse description
      const textElements = $('.elementor-widget-text-editor', widget)
      const descriptionHTML = $('.elementor-clearfix', textElements[0]).html()
        .replace(/&#xA0;/g, '')
        .replace(/<div><\/div>/g, '')
        .replace(/<p><\/p>/g, '')

      const mainDescription = converter
        .makeMarkdown(descriptionHTML, dom.window.document)
        .replace(/<br>/g, '')
        .trim()

      description += `${mainDescription}\n\n##### Anmeldung unter\nKontakt[ät]prozesswerkstatt-leipzig.org\n\n\n`
    } else if ($(widget).html().match(/>Kosten<\/h1>/)) {
      const el = $('.elementor-clearfix', widget)
      const costsHTML = el.html()
        .replace(/&#xA0;/g, '')
        .replace(/<div><\/div>/g, '')
        .replace(/<br><\/strong>/g, '</strong><br>')
        .replace(/<p><\/p>/g, '')

      const costs = converter
        .makeMarkdown(costsHTML, dom.window.document)
        .replace(/<br>/g, '')
        .trim()

      description += `#### Kosten\n${costs}\n\n\n`

      // trichter disclamer
      description += '> **Anmerkung [C. // trichter]**\n'
      description += '> _Hier scheint ein Mindestpreis für die Teilnahme festgelegt zu sein, was erstmal eigentlich nicht tauschlogikfrei ist und damit den Kriterien für trichter Veranstaltungen widerspricht. Das Angebot mit den Veranstalter\\*innen ins Gespräch über eine geldfreie Teilnahme zu kommen kann für Menschen mit wenig finanziellen Mittel eine große Hürde darstellen. Die Gruppe macht sich aber durchaus Mühe es für alle Menschen möglichst inklusiv zu gestalten und Bedürfnisse von Menschen zu hören um auf diese einzugehen. Denk gerne darüber nach ob es für dich "tauschlogikfrei" ist. Wir finden: Es ist definitiv ein emanzipatorischen Ansatz, den wir unterstützenswert finden!_\n\n'

    } else if ($(widget).html().match(/Kleingedruckte/)) {
      const el = $('.elementor-clearfix', widget)
      const kleingedruckteHTML = el.html()
        .replace(/&#xA0;/g, '')
        .replace(/<div><\/div>/g, '')
        .replace(/<br><\/strong>/g, '</strong><br>')
        .replace(/<p><\/p>/g, '')

      const kleingedruckte = converter
        .makeMarkdown(kleingedruckteHTML, dom.window.document)
        .replace(/<br>/g, '')
        .trim()

      description += `#### Das "Kleingedruckte"\n${kleingedruckte}`
    }
  }

  const id = url.split('/')[3] + startDate.format('-YYYY-MM')
  const event: ScrapedEvent = {
    id,
    title,
    start: startDate.toISOString(),
    end: endDate.toISOString(),
    address,
    link: url,
    description: description,
    locationName: 'Leipzig'
  }

  // get image
  const postCSSUrl = body.match(/https:\/\/www\.prozesswerkstatt-leipzig\.org\/wp-content\/uploads\/elementor\/css\/post-\d+\.css/)
  if (postCSSUrl) {
    const postCssRes = await fetch(postCSSUrl[0])
    const bgImage = (await postCssRes.text()).match(/background-image:url\("(.*?)"/)
    if (bgImage) {
      const url = new URL(bgImage[1])
      const imageRes = await fetch(url.href)
      const contentType = imageRes.headers.get('content-type')
      if (contentType && contentType.match(/^image\/(jpeg|png)$/)) {
        const image = await imageRes.buffer()
        event.imageData = image.toString('base64')
        event.image = basename(url.pathname)
      } else {
        log.warn('got invalid content type for image', contentType)
      }
    }
  }
  return event
}
