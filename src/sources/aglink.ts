import * as moment from 'moment'
import { ScrapeFunction, ScrapedEvent } from '../index'
import Logger from '@trichter/logger'
import fetch from '../fetch'
import { parse as xmlParse } from 'fast-xml-parser'
import * as showdown from 'showdown'
import * as jsdom from 'jsdom'

const log = Logger('trichter-scraper/aglink')
const converter = new showdown.Converter({
  encodeEmails: false
})
const dom = new jsdom.JSDOM()

interface IFeedEntry {
  title: string
  link: string
  published: string
  updated: string
  id: string
  content: string
  author: any
  summary: string
}

// https://stackoverflow.com/a/17076120
function decodeHTMLEntities (text: string): string {
  var entities = [
    ['amp', '&'],
    ['apos', '\''],
    ['#x27', '\''],
    ['#x2F', '/'],
    ['#39', '\''],
    ['#47', '/'],
    ['lt', '<'],
    ['gt', '>'],
    ['nbsp', ' '],
    ['quot', '"']
  ]

  for (var i = 0, max = entities.length; i < max; ++i) { text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]) }

  return text
}

const crawl: ScrapeFunction = async (options?: {}) => {
  try {
    const res = await fetch('https://ag-link.xyz/feed.xml')
    const body = await res.text()

    const events: ScrapedEvent[] = []

    const data = xmlParse(body)
    for (const entry of data.feed.entry as IFeedEntry[]) {
      if (entry.id.indexOf('/event/') !== 0) continue

      const description = converter.makeMarkdown(decodeHTMLEntities(entry.content), dom.window.document).replace(/<br>/g, '')

      // is date in the past?
      if (moment(entry.published).isBefore(moment())) continue

      const r = description.match(/findet am .*? um (?<hour>[0-9]{2}):(?<minutes>[0-9]{2}) Uhr (in|im) (?<location>.*?) statt/)

      if (!r) {
        log.warn(`could not get time and location of ${entry.id}`)
        continue
      }

      const event: ScrapedEvent = {
        id: entry.id,
        title: entry.title,
        start: entry.published.replace(/T\d{2}:\d{2}/, `T${r.groups.hour}:${r.groups.minutes}`),
        end: null,
        address: r.groups.location,
        link: `https://ag-link.xyz${entry.id}.html`,
        teaser: entry.summary.replace(/\n/mg, ''),
        description: description
      }
      events.push(event)
    }
    return events
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl
