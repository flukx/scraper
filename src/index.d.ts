export interface ScrapedEvent {
  id: string
  parentId?: string
  title: string
  start: string
  end: string
  locationName?: string
  address: string
  link?: string
  image?: string
  imageData?: string
  teaser?: string
  description?: string
}
export type ScrapeFunction = (options: any) => Promise<ScrapedEvent[]>
